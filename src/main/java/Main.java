import Dokumenty.Application;
import Dokumenty.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jan_w on 01.10.2017.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String entryLine = "";

        while (!entryLine.equals("quit")){

            System.out.println("Podaj imię, nazwisko, numer indeksu:");
            String[] personData = scanner.nextLine().split(" ");
            Person person = new Person(personData[0], personData[1], personData[2]);

            System.out.println("Wybierz rodzaj formularza, który chcesz wypełnić: extend / condition / schoolarship / social");
            String formularz = scanner.nextLine();

            switch (formularz){
                case "extend":
                    System.out.println("Wpisz treść: ");
                    String text = scanner.nextLine();
                    System.out.println();
                    System.out.println("Wpisz powód: ");
                    String reason = scanner.nextLine();
                    System.out.println(AppFactory.createSemesterExtendApp(person, text,reason));
                    break;

                case "condition":
                    System.out.println("Wpisz treść: ");
                    String text2 = scanner.nextLine();
                    System.out.println("Podaj swoje oceny po przecinkach: ");
                    String[] gradesTable = scanner.nextLine().split(", ");
                    List<Double> gradesList = new ArrayList<>();
                    for (String grade: gradesTable){
                        gradesList.add(Double.parseDouble(grade));
                    }
                    System.out.println("Wpisz powód: ");
                    String reason2 = scanner.nextLine();
                    System.out.println(AppFactory.createConditionalStayApp(person,text2, gradesList, reason2));
                    break;

                default:
                    System.out.println("Niepoprawne polecenie.");
            }
            System.out.println("Wpisz 'quit' aby zamnknąć program lub 'następny' by wypełnić kolejny wniosek.");
            entryLine = scanner.nextLine();

        }
    }

}
