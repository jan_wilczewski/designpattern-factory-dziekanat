package Dokumenty;

import java.time.LocalDateTime;

/**
 * Created by jan_w on 01.10.2017.
 */
public class Application {

    private LocalDateTime dataUtworzenia;
    private String miejsceUtworzenia;
    private Person daneAplikanta;
    private String tresc;

    public Application(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        this.dataUtworzenia = dataUtworzenia;
        this.miejsceUtworzenia = miejsceUtworzenia;
        this.daneAplikanta = daneAplikanta;
        this.tresc = tresc;
    }

    @Override
    public String toString() {
        return "Application{" +
                "dataUtworzenia=" + dataUtworzenia +
                ", miejsceUtworzenia='" + miejsceUtworzenia + '\'' +
                ", daneAplikanta=" + daneAplikanta +
                ", tresc='" + tresc + '\'' +
                '}';
    }
}
