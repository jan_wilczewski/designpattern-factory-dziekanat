package Dokumenty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jan_w on 01.10.2017.
 */
public class ConditionalStayApplication extends Application {

    private List<Double> grades = new ArrayList<>();
    private String reason;

    public ConditionalStayApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, List<Double> grades, String reason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.reason = reason;
    }


}
