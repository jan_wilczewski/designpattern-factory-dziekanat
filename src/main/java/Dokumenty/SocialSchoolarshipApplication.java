package Dokumenty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jan_w on 01.10.2017.
 */
public class SocialSchoolarshipApplication extends Application{

    private List<Double> grades = new ArrayList<>();
    private double totalFamilyIncome;

    public SocialSchoolarshipApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, List<Double> grades, double totalFamilyIncome) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.grades = grades;
        this.totalFamilyIncome = totalFamilyIncome;
    }
}
