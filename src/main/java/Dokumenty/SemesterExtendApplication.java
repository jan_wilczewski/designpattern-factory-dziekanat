package Dokumenty;

import java.time.LocalDateTime;

/**
 * Created by jan_w on 01.10.2017.
 */
public class SemesterExtendApplication extends Application {

    private String rason;

    public SemesterExtendApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc, String rason) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
        this.rason = rason;
    }
}
