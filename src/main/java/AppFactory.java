import Dokumenty.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by jan_w on 01.10.2017.
 */
public class AppFactory {

    public static Application createSemesterExtendApp(Person person, String tresc, String reason){
        return new SemesterExtendApplication(LocalDateTime.now(), "Gdynia", person, tresc, reason);
    }

    public static Application createConditionalStayApp(Person person, String tresc, List<Double> grades, String reason){
        return new ConditionalStayApplication(LocalDateTime.now(), "Gdynia", person, tresc, grades, reason);
    }

    public static Application createSchoolarshipApp (Person person, String tresc, List<Double> grades, List<String> extraActiv){
        return new SchoolarshipApplication(LocalDateTime.now(), "Gdynia", person, tresc, grades, extraActiv);
    }

    public static Application createSocialApp (Person person, String tresc, List<Double> grades, double income) {
        return new SocialSchoolarshipApplication(LocalDateTime.now(), "Gdynia", person, tresc, grades, income);
    }

}
