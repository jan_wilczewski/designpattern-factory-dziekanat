import Dokumenty.Application;
import Dokumenty.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jan_w on 01.10.2017.
 */
public class MainVersion2 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Podaj imię i nazwisko oraz numer indeksu: ");
        String[] personDetails = scanner.nextLine().split(" ");
        Person person = new Person(personDetails[0], personDetails[1], personDetails[2]);

        System.out.println("Jaki typ formularza chcesz wypełnić? (Warunek/StypendiumNaukowe/StypendiumSocjalne/WydluzenieSemestru)");
        String typFormularza = scanner.nextLine();

        System.out.println("Podaj swoje oceny oddzielone przecinakmi: ");
        List<Double> grades = new ArrayList<>();
        String[] gradesList = scanner.nextLine().split(",");
        for (String grade : gradesList) {
            grades.add(Double.parseDouble(grade));
        }

        Application a = null;

        if (typFormularza.equals("Warunek")) {
            System.out.println("Wpisz treść: ");
            String text = scanner.next();
            System.out.println("Podaj powód dlaczego miałbyś zostać?");
            String reason = scanner.next();
            a = AppFactory.createConditionalStayApp(person, text, grades, reason);

        } else if (typFormularza.equals("StypendiumNaukowe")) {
            System.out.println("Wpisz treść: ");
            String text = scanner.next();
            System.out.println("Wymień Twoje zajęcia dodatkowe po przecinku:");
            String[] zajecia = scanner.nextLine().split(",");
            List<String> listaExtra = new ArrayList<>();
            for (String zaj : zajecia) {
                listaExtra.add(zaj);
            }
            a = AppFactory.createSchoolarshipApp(person, text, grades, listaExtra);

        } else if (typFormularza.equals("StypendiumSocjalne")) {
            System.out.println("Wpisz treść: ");
            String text = scanner.next();
            System.out.println("Podaj średni dochód na członka rodziny: ");
            double totalFamilyIncome = scanner.nextDouble();
            a = AppFactory.createSocialApp(person, text, grades, totalFamilyIncome);

        } else if (typFormularza.equals("WydluzenieSemestru")) {
            System.out.println("Wpisz treść: ");
            String text = scanner.next();
            System.out.println("Podaj powód dlaczego miałbyś zostać?");
            String reason = scanner.next();
            a = AppFactory.createSemesterExtendApp(person, text, reason);

        } else {
            System.out.println("Wybrałeś zły rodzaj formularza, uruchom program ponownie");

        }
        System.out.println(a);
        System.out.println("Dziękuję, Twój formularz został wysłany");
    }

}
